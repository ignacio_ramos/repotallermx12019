var clientesObtenidos;
function getCliente() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers"; //?$filter=Country eq 'Germany'";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      //console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarProductos();
    }
  }

  request.open("GET",url,true);
  request.send();
}

function procesarProductos() {
  var JSONProductos = JSON.parse(clientesObtenidos);
  var urlBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  //alert(JSONProductos.value[0].ProductName);

  var divTabla = document.getElementById("tablaClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONProductos.value.length; i++) {
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");
    var columnaContactName = document.createElement("td");
    columnaContactName.innerText = JSONProductos.value[i].ContactName;
    var columnaCity = document.createElement("td");
    columnaCity.innerText = JSONProductos.value[i].City;

    var banderaNombre = JSONProductos.value[i].Country;
    
    if(banderaNombre == "UK") {
      var uk = "United-Kingdom";
      banderaNombre = "United-Kingdom";
      var bandera = urlBandera + banderaNombre+".png";
    } else {
      var bandera = urlBandera + JSONProductos.value[i].Country+".png";
    }
    //var bandera = urlBandera + JSONProductos.value[i].Country+".png";
    var columnaCountry = document.createElement("td");
    var img = document.createElement("img");
    img.classList.add("flag");
    img.src = bandera;
    columnaCountry.appendChild(img);

    nuevaFila.appendChild(columnaContactName);
    nuevaFila.appendChild(columnaCity);
    nuevaFila.appendChild(columnaCountry);
    //nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
